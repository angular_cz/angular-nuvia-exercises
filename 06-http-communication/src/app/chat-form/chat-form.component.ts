import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Message } from '../model/chat';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-chat-form',
  templateUrl: './chat-form.component.html',
  styleUrls: ['./chat-form.component.css']
})
export class ChatFormComponent implements OnInit {

  userForm: FormGroup;

  @Output()
  sendMessage = new EventEmitter<Message>();

  constructor(private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.userForm = this.formBuilder.group({
      user: ['', Validators.required],
      text: ['', Validators.required]
    });
  }

  handleSubmit({valid, value}) {
    if (!valid) {
      alert('Fill name & message!');
    } else {
      this.sendMessage.emit(value);
      this.userForm.reset();
    }
  }

}
