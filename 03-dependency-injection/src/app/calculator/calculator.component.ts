import { Component } from '@angular/core';
import { Book } from '../model/book';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html'
})
export class CalculatorComponent {

  book: Book;

  // TODO 1.2 - injektněte službu do komponenty
  // TODO 2.3 - injektněte službu do komponenty
  constructor() {
    this.clearCalculator();
  }

  getPrice() {
    // TODO 1.3 - provolejte metodu služby
    return this.calculate(this.book);
  }

  // TODO 1.3 - přesuňte výpočet do služby
  calculate(book: Book) {

    const colorPagePrice = 10;
    const normalPagePrice = 1;
    const letterOnCoverPrice = 2;

    let price = 0;

    const normalPages = book.totalPages - book.colorPages;

    price += book.title.length * letterOnCoverPrice;
    price += normalPages * normalPagePrice;
    price += book.colorPages * colorPagePrice;

    return price;
  }

  save() {
    // TODO 2.3 - provolejte metodu služby
    this.saveCalculation(this.book);
    this.clearCalculator();
  }

  getCalculations() {
    // TODO 2.3 - získejte vlastnost ze služby
    return this.calculations;
  }

  // TODO 2.2 - přesuňte ukládané výpočty do služby
  // noinspection TsLint
  calculations: Book[] = [];

  saveCalculation(book: Book) {
    book.price = this.calculate(book);
    this.calculations.push(book);
  }

  private clearCalculator() {
    this.book = {
      title: 'Book title',
      totalPages: 40,
      colorPages: 0
    };
  }

}
