import { Component, Input, Host } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';

@Component({
  selector: 'app-form-model-errors',
  templateUrl: './form-model-errors.component.html'
})
export class FormModelErrorsComponent {

  @Input()
  control: FormControl;

  constructor(@Host() public ngForm: NgForm) {
  }

  shouldDisplayErrors() {
    return this.control.touched || this.ngForm.submitted;
  }

  get errors() {
    return this.control.errors;
  }

}
