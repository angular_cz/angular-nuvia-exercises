import {Component, Input} from '@angular/core';
import { Address } from '../model/order';

@Component({
  selector: 'app-order-address',
  templateUrl: './order-address.component.html'
})

export class OrderAddressComponent {

  // TODO 2.1 - dekorujte vlastnosti
  title = 'Address';
  address: Address;

}
