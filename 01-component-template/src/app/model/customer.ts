export interface Address {
  street: string;
  city: string;
  state: string;
}

export interface Customer {
  name: string;
  email: string;
  score: number;

  address: Address;
}
