import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChatComponent } from './chat.component';
import { RoomsComponent } from './rooms/rooms.component';
import { ChatDetailComponent } from './chat-detail/chat-detail.component';
import { RoomsResolver } from './rooms-resolver.service';

const routes: Routes = [
  {
    path: '', component: ChatComponent,
    children: [
      {path: '', redirectTo: 'rooms', pathMatch: 'full'},
      {path: 'rooms', component: RoomsComponent, resolve: {rooms: RoomsResolver}},
      {path: 'room/:id', component: ChatDetailComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [RoomsResolver]
})
export class ChatRoutingModule {
}
