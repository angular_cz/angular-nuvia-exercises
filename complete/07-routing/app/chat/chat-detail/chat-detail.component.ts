import { Component, OnInit } from '@angular/core';
import { ChatRoom, Message } from '../model/chat';

import { interval, merge, Observable, Subject } from 'rxjs';
import { startWith, flatMap, tap } from 'rxjs/operators';

import { ActivatedRoute, Params } from '@angular/router';
import { ChatService } from '../chat.service';

@Component({
    selector: 'app-chat-detail',
    templateUrl: './chat-detail.component.html'
})
export class ChatDetailComponent implements OnInit {

    chatRoomId = 1;
    chatRoom: ChatRoom;
    myMessages$ = new Subject<Message>();
    myMessagesStream$: Observable<Response>;
    routeParams$: Observable<Params>;

    constructor(private chatService: ChatService,
                private route: ActivatedRoute) {

        this.routeParams$ = this.route.params
            .pipe(
                tap(params => this.chatRoomId = parseInt(params['id']))
            );

        this.myMessagesStream$ = this.myMessages$
            .pipe(
                flatMap(message => this.chatService.sendMessage(this.chatRoomId, message))
            );
    }

    ngOnInit(): void {

        merge(interval(5000),
            this.myMessagesStream$,
            this.routeParams$
        ).pipe(
            startWith(null),
            flatMap(() => this.chatService.getRoomById(this.chatRoomId)),
            tap(() => console.log('chatReloaded'))
        )

            .subscribe((chatRoom) => this.chatRoom = chatRoom);
    }

    sendMyMessage(message: Message) {
        this.myMessages$.next(message);
    }
}
