import { Component, OnInit } from '@angular/core';
import { Customer } from '../model/customer';

@Component({
    selector: 'app-customer',
    templateUrl: './customer.component.html',
    styleUrls: ['./customer.component.css']
})
export class CustomerComponent {

    customer: Customer = {
        name: 'Karl Montegri',
        email: 'karl.m@motasco.org',
        score: 2,
        address: {
            street: 'Jason park 12',
            city: 'Felt',
            state: 'Idaho'
        }
    };

    getCustomerRank() {
        if (this.customer.score > 3) {
            return 'VIP';
        }
        if (this.customer.score > 0) {
            return 'regular';
        }

        return 'unknown';
    }

    increase() {
        this.customer.score++;
    }

    decrease() {
        this.customer.score--;
    }

}
