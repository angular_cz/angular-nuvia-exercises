import { Component, Host, OnInit, Input, AfterContentInit, AfterViewInit, OnDestroy } from '@angular/core';
import { ControlContainer, FormGroup, NgForm } from '@angular/forms';
import { Subscription, merge } from 'rxjs';

@Component({
  selector: 'app-form-control-errors',
  templateUrl: './form-control-errors.component.html',
  styleUrls: ['./form-control-errors.component.css']
})
export class FormControlErrorsComponent implements OnInit, OnDestroy {

  @Input() controlName;

  formDirective: any;
  formGroup: FormGroup;

  subscription: Subscription;

  errors: any = {};

  constructor(@Host() public controlContainer: ControlContainer) {
  }

  ngOnInit(): void {
    const controlPath = [...this.controlContainer.path, this.controlName];
    // TODO otestovat pro větší zanoření

    this.formDirective = this.controlContainer.formDirective;
    this.formGroup = this.formDirective.form as FormGroup;

    this.subscription = merge(this.formDirective.ngSubmit, this.formGroup.statusChanges)
      .subscribe(() => {
        // TODO nelze použít touched, protože se nastavuje až po propagaci statusChanges
        console.log('errors!!!', controlPath, this.formGroup.touched);

        const control = this.formGroup.get(controlPath);
        if (control) {
          this.errors = control.errors;
        } else {
          this.errors = {};
        }
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
