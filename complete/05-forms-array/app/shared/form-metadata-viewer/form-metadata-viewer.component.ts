import { Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-form-metadata-viewer',
  templateUrl: 'form-metadata-viewer.component.html'
})
export class FormMetadataViewerComponent {

  @Input() control: FormControl;

}
