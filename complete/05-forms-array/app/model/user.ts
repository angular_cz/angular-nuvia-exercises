export interface Contact {
  phone: string;
}

export interface User {
  name: string;
  surname: string;
  twitterName?: string;
  contacts: Contact[];
  registrationDate?: Date;
}
