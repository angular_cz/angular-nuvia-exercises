import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { User } from '../../model/user';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';

@Component({
  selector: 'app-user-reactive-form',
  templateUrl: './user-reactive-form.component.html',
  styleUrls: ['./user-reactive-form.component.css']
})
export class UserReactiveFormComponent implements OnInit {

  @Input() user: User;

  @Output() userSubmit = new EventEmitter<User>();

  userForm: FormGroup;

  constructor(private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.userForm = this.formBuilder.group({
      name: ['', Validators.required],
      surname: ['', Validators.required],
      contacts: this.formBuilder.array([])
    });

    this.addMultipleAddresses(this.user.contacts.length);

    this.userForm.patchValue(this.user);
  }

  initContact() {
    return this.formBuilder.group({
      phone: ['', [Validators.required, Validators.minLength(9)]]
    });
  }

  addMultipleAddresses(count: number) {
    for (; count > 0; count--) {
      this.addAddress();
    }
  }

  addAddress() {
    // add address to the list
    const control = this.userForm.controls['contacts'] as FormArray;
    control.push(this.initContact());
  }

  removeAddress(i: number) {
    // remove address from the list
    const control = this.userForm.controls['contacts'] as FormArray;
    control.removeAt(i);
  }


  onSubmit(form) {
    if (form.valid) {
      this.userSubmit.emit(form.value);
    }
  }

}
