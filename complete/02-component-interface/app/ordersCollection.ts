export const orders = [
    {
        'totalPrice': 103980,
        'orderedItems': [
            {
                'totalPrice': 61936,
                'pricePerPiece': 784,
                'quantity': 79,
                'productName': 'aliqua',
                'productId': '#208885'
            },
            {
                'totalPrice': 4370,
                'pricePerPiece': 115,
                'quantity': 38,
                'productName': 'excepteur',
                'productId': '#269149'
            },
            {
                'totalPrice': 2145,
                'pricePerPiece': 715,
                'quantity': 3,
                'productName': 'commodo',
                'productId': '#740208'
            },
            {
                'totalPrice': 30625,
                'pricePerPiece': 875,
                'quantity': 35,
                'productName': 'deserunt',
                'productId': '#350504'
            },
            {
                'totalPrice': 4904,
                'pricePerPiece': 613,
                'quantity': 8,
                'productName': 'ad',
                'productId': '#218474'
            }
        ],
        'created': 'Sunday, March 30, 2014 1:06 PM',
        'note': 'Excepteur nulla in mollit sit ex minim duis. Eu et qui velit ullamco eu. Dolore sit irure irure amet sunt.',
        'phone': '+489 837 359 688',
        'email': 'crawford guzman@injoy.io',
        'billingAddress': {
            'state': 'Oregon',
            'city': 'Torboy',
            'address': 'Desmond Court 703',
            'contact': 'Maude Hamilton',
            'company': 'GYNKO'
        },
        'shippingAddress': {
            'state': 'Alaska',
            'city': 'Floris',
            'address': 'Amboy Street 162',
            'contact': 'Crawford Guzman',
            'company': 'INJOY'
        },
        'guid': '5c72dd39-65c2-4b89-8b2e-96be61c2cae3'
    },
    {
        'totalPrice': 185383,
        'orderedItems': [
            {
                'totalPrice': 18186,
                'pricePerPiece': 866,
                'quantity': 21,
                'productName': 'adipisicing',
                'productId': '#991360'
            },
            {
                'totalPrice': 65044,
                'pricePerPiece': 707,
                'quantity': 92,
                'productName': 'do',
                'productId': '#607908'
            },
            {
                'totalPrice': 68985,
                'pricePerPiece': 945,
                'quantity': 73,
                'productName': 'excepteur',
                'productId': '#655122'
            },
            {
                'totalPrice': 23324,
                'pricePerPiece': 476,
                'quantity': 49,
                'productName': 'velit',
                'productId': '#523086'
            },
            {
                'totalPrice': 9844,
                'pricePerPiece': 214,
                'quantity': 46,
                'productName': 'veniam',
                'productId': '#430700'
            }
        ],
        'created': 'Monday, August 3, 2015 10:07 PM',
        'note': 'Veniam exercitation amet id adipisicing labore. Amet enim tempor mollit id officia sint proident pariatur consequat. Adipisicing labore duis occaecat anim mollit duis occaecat laboris non enim non irure sint laborum. Excepteur tempor tempor laboris incididunt veniam culpa amet. Laborum do culpa ex elit eu consectetur non elit dolore pariatur dolor voluptate consectetur nisi. Sunt qui Lorem labore nisi occaecat amet occaecat do duis amet anim cillum. Veniam voluptate consectetur aliquip minim deserunt cupidatat reprehenderit ipsum in enim dolore ea.',
        'phone': '+430 235 487 358',
        'email': 'farley silva@lexicondo.com',
        'billingAddress': {
            'state': 'Minnesota',
            'city': 'Felt',
            'address': 'Tudor Terrace 953',
            'contact': 'Nell Dennis',
            'company': 'ZENTILITY'
        },
        'shippingAddress': {
            'state': 'Missouri',
            'city': 'Crucible',
            'address': 'Dorset Street 390',
            'contact': 'Farley Silva',
            'company': 'LEXICONDO'
        },
        'guid': '4df42bfe-f35c-4acc-b174-a7b7b074b781'
    },
    {
        'totalPrice': 111076,
        'orderedItems': [
            {
                'totalPrice': 23670,
                'pricePerPiece': 526,
                'quantity': 45,
                'productName': 'proident',
                'productId': '#345352'
            },
            {
                'totalPrice': 6890,
                'pricePerPiece': 265,
                'quantity': 26,
                'productName': 'nisi',
                'productId': '#290427'
            },
            {
                'totalPrice': 32376,
                'pricePerPiece': 426,
                'quantity': 76,
                'productName': 'amet',
                'productId': '#464761'
            },
            {
                'totalPrice': 48140,
                'pricePerPiece': 580,
                'quantity': 83,
                'productName': 'consectetur',
                'productId': '#955830'
            }
        ],
        'created': 'Wednesday, October 12, 2016 4:21 AM',
        'note': 'Ad cupidatat nulla ullamco proident excepteur dolore non nisi enim mollit esse ullamco. Labore exercitation laborum et reprehenderit in ea quis aliqua aliqua esse officia eu anim. Nulla eu ex id anim veniam officia nisi sunt et.',
        'phone': '+452 559 490 228',
        'email': 'lacey love@vixo.biz',
        'billingAddress': {
            'state': 'Palau',
            'city': 'Graniteville',
            'address': 'Howard Avenue 956',
            'contact': 'Marisa Stokes',
            'company': 'MARKETOID'
        },
        'guid': 'bf5d149a-4ca5-4967-9e01-ba88c2574cf4'
    },
    {
        'totalPrice': 106807,
        'orderedItems': [
            {
                'totalPrice': 481,
                'pricePerPiece': 481,
                'quantity': 1,
                'productName': 'ex',
                'productId': '#192264'
            },
            {
                'totalPrice': 21371,
                'pricePerPiece': 301,
                'quantity': 71,
                'productName': 'ullamco',
                'productId': '#611879'
            },
            {
                'totalPrice': 60705,
                'pricePerPiece': 639,
                'quantity': 95,
                'productName': 'eiusmod',
                'productId': '#363128'
            },
            {
                'totalPrice': 17524,
                'pricePerPiece': 337,
                'quantity': 52,
                'productName': 'nisi',
                'productId': '#624386'
            },
            {
                'totalPrice': 6726,
                'pricePerPiece': 177,
                'quantity': 38,
                'productName': 'adipisicing',
                'productId': '#543953'
            }
        ],
        'created': 'Tuesday, August 19, 2014 11:13 AM',
        'note': 'Reprehenderit nostrud ex do magna minim voluptate commodo elit non. Do duis elit cupidatat esse esse non irure consequat nostrud. Mollit irure voluptate tempor eu veniam sunt ipsum culpa incididunt deserunt qui. Consequat sint amet eiusmod cillum fugiat sint culpa voluptate deserunt enim eu deserunt ullamco ex. Ad minim anim exercitation voluptate magna excepteur ea commodo magna officia Lorem proident Lorem proident. Mollit elit veniam reprehenderit ex magna dolore ipsum id pariatur veniam. Cupidatat mollit duis consequat dolore reprehenderit laboris ipsum ad non non non laboris esse.',
        'phone': '+426 273 698 926',
        'email': 'gibbs ross@satiance.us',
        'billingAddress': {
            'state': 'Maine',
            'city': 'Churchill',
            'address': 'Central Avenue 185',
            'contact': 'Buckner Dunlap',
            'company': 'BALOOBA'
        },
        'shippingAddress': {
            'state': 'California',
            'city': 'Hemlock',
            'address': 'Eldert Street 985',
            'contact': 'Gibbs Ross',
            'company': 'SATIANCE'
        },
        'guid': 'f68dad16-f782-4fff-bf38-4db5583f812f'
    },
    {
        'totalPrice': 116194,
        'orderedItems': [
            {
                'totalPrice': 10804,
                'pricePerPiece': 292,
                'quantity': 37,
                'productName': 'mollit',
                'productId': '#743448'
            },
            {
                'totalPrice': 28420,
                'pricePerPiece': 406,
                'quantity': 70,
                'productName': 'esse',
                'productId': '#452025'
            },
            {
                'totalPrice': 76970,
                'pricePerPiece': 895,
                'quantity': 86,
                'productName': 'ad',
                'productId': '#311349'
            }
        ],
        'created': 'Wednesday, September 2, 2015 11:42 PM',
        'note': 'Ipsum magna ullamco laborum nulla amet sint consectetur aliqua culpa sint veniam consectetur ea minim. Aute aliqua excepteur nisi quis nostrud proident aliqua aute dolore qui nisi velit et. Dolore consectetur sit non ut laboris ad minim pariatur enim reprehenderit nostrud esse reprehenderit proident. Mollit occaecat eu ut id. Irure laboris mollit id ipsum elit. Voluptate ex id quis irure est aute.',
        'phone': '+496 146 652 422',
        'email': 'rosemary rojas@combogene.net',
        'billingAddress': {
            'state': 'Nebraska',
            'city': 'Salvo',
            'address': 'Orient Avenue 992',
            'contact': 'Mitchell Trevino',
            'company': 'HOMELUX'
        },
        'shippingAddress': {
            'state': 'Illinois',
            'city': 'Bascom',
            'address': 'Albemarle Terrace 386',
            'contact': 'Rosemary Rojas',
            'company': 'COMBOGENE'
        },
        'guid': 'bd7e3c9c-baa3-4933-90f0-f547a2447750'
    },
    {
        'totalPrice': 88869,
        'orderedItems': [
            {
                'totalPrice': 13536,
                'pricePerPiece': 376,
                'quantity': 36,
                'productName': 'exercitation',
                'productId': '#944811'
            },
            {
                'totalPrice': 12974,
                'pricePerPiece': 499,
                'quantity': 26,
                'productName': 'aliquip',
                'productId': '#457662'
            },
            {
                'totalPrice': 14239,
                'pricePerPiece': 491,
                'quantity': 29,
                'productName': 'irure',
                'productId': '#501958'
            },
            {
                'totalPrice': 23680,
                'pricePerPiece': 320,
                'quantity': 74,
                'productName': 'reprehenderit',
                'productId': '#671927'
            },
            {
                'totalPrice': 24440,
                'pricePerPiece': 611,
                'quantity': 40,
                'productName': 'dolore',
                'productId': '#251736'
            }
        ],
        'created': 'Monday, April 25, 2016 10:26 PM',
        'note': 'Ea occaecat dolor consectetur anim velit cillum incididunt. Et enim nisi labore minim cupidatat nisi. Nulla sit exercitation consequat velit exercitation sit sunt adipisicing elit nisi ipsum.',
        'phone': '+454 908 444 661',
        'email': 'skinner warren@enersol.co.uk',
        'billingAddress': {
            'state': 'Louisiana',
            'city': 'Camptown',
            'address': 'Cortelyou Road 482',
            'contact': 'Hunt Kemp',
            'company': 'FURNITECH'
        },
        'guid': '22a83308-86d8-476d-8230-b3e94401d4e1'
    },
    {
        'totalPrice': 112278,
        'orderedItems': [
            {
                'totalPrice': 10282,
                'pricePerPiece': 194,
                'quantity': 53,
                'productName': 'anim',
                'productId': '#982691'
            },
            {
                'totalPrice': 30711,
                'pricePerPiece': 353,
                'quantity': 87,
                'productName': 'reprehenderit',
                'productId': '#606318'
            },
            {
                'totalPrice': 18972,
                'pricePerPiece': 306,
                'quantity': 62,
                'productName': 'adipisicing',
                'productId': '#708112'
            },
            {
                'totalPrice': 18473,
                'pricePerPiece': 377,
                'quantity': 49,
                'productName': 'laboris',
                'productId': '#773811'
            },
            {
                'totalPrice': 33840,
                'pricePerPiece': 705,
                'quantity': 48,
                'productName': 'aute',
                'productId': '#622290'
            }
        ],
        'created': 'Tuesday, April 12, 2016 9:23 PM',
        'note': 'Irure proident exercitation dolore officia qui. Eu sint sint incididunt veniam est ullamco cupidatat excepteur. Occaecat aliqua adipisicing consectetur aute et adipisicing ad nostrud.',
        'phone': '+456 286 570 148',
        'email': 'oneill berg@cogentry.me',
        'billingAddress': {
            'state': 'Ohio',
            'city': 'Terlingua',
            'address': 'Dunne Place 115',
            'contact': 'Suzanne Wolf',
            'company': 'ECLIPTO'
        },
        'shippingAddress': {
            'state': 'New Jersey',
            'city': 'Leland',
            'address': 'Willoughby Avenue 364',
            'contact': 'Oneill Berg',
            'company': 'COGENTRY'
        },
        'guid': 'f86f381e-b1e1-4901-9eda-34d2dc88a723'
    },
    {
        'totalPrice': 82615,
        'orderedItems': [
            {
                'totalPrice': 22876,
                'pricePerPiece': 817,
                'quantity': 28,
                'productName': 'irure',
                'productId': '#727273'
            },
            {
                'totalPrice': 53979,
                'pricePerPiece': 947,
                'quantity': 57,
                'productName': 'qui',
                'productId': '#486950'
            },
            {
                'totalPrice': 5760,
                'pricePerPiece': 576,
                'quantity': 10,
                'productName': 'pariatur',
                'productId': '#522300'
            }
        ],
        'created': 'Saturday, December 20, 2014 7:30 PM',
        'note': 'Aliqua sunt fugiat culpa ut. Sunt officia enim sit sunt ipsum deserunt elit. Ad anim ea eiusmod dolore reprehenderit aliquip sit excepteur velit nulla. Lorem consectetur adipisicing deserunt anim dolor non labore commodo labore do elit et. Laboris culpa nulla consectetur proident officia amet excepteur irure. Occaecat voluptate sunt proident in eiusmod.',
        'phone': '+429 444 393 294',
        'email': 'chapman riley@earthwax.tv',
        'billingAddress': {
            'state': 'South Carolina',
            'city': 'Bend',
            'address': 'Debevoise Avenue 824',
            'contact': 'Palmer Berry',
            'company': 'FISHLAND'
        },
        'shippingAddress': {
            'state': 'Wyoming',
            'city': 'Sunriver',
            'address': 'Oxford Walk 559',
            'contact': 'Chapman Riley',
            'company': 'EARTHWAX'
        },
        'guid': '3abde129-7e03-4da3-a29a-900d3fa02b2e'
    },
    {
        'totalPrice': 113676,
        'orderedItems': [
            {
                'totalPrice': 9408,
                'pricePerPiece': 448,
                'quantity': 21,
                'productName': 'quis',
                'productId': '#646358'
            },
            {
                'totalPrice': 67284,
                'pricePerPiece': 756,
                'quantity': 89,
                'productName': 'pariatur',
                'productId': '#823986'
            },
            {
                'totalPrice': 17775,
                'pricePerPiece': 395,
                'quantity': 45,
                'productName': 'tempor',
                'productId': '#282371'
            },
            {
                'totalPrice': 10440,
                'pricePerPiece': 870,
                'quantity': 12,
                'productName': 'sint',
                'productId': '#536803'
            },
            {
                'totalPrice': 7040,
                'pricePerPiece': 704,
                'quantity': 10,
                'productName': 'laboris',
                'productId': '#405639'
            },
            {
                'totalPrice': 1729,
                'pricePerPiece': 247,
                'quantity': 7,
                'productName': 'ipsum',
                'productId': '#451103'
            }
        ],
        'created': 'Wednesday, August 6, 2014 8:47 AM',
        'note': 'Ipsum deserunt incididunt non occaecat. Commodo magna eiusmod veniam veniam fugiat sunt reprehenderit irure est tempor. Ipsum ad nostrud qui mollit ut adipisicing in. Mollit cupidatat pariatur voluptate sit proident culpa nisi.',
        'phone': '+459 258 889 521',
        'email': 'dee tanner@telequiet.info',
        'billingAddress': {
            'state': 'Nevada',
            'city': 'Noblestown',
            'address': 'John Street 565',
            'contact': 'Bowman Strickland',
            'company': 'ATOMICA'
        },
        'shippingAddress': {
            'state': 'New Mexico',
            'city': 'Jacumba',
            'address': 'Bergen Court 620',
            'contact': 'Dee Tanner',
            'company': 'TELEQUIET'
        },
        'guid': '2f77330d-1fd0-44b7-893f-c6f73f87d97d'
    },
    {
        'totalPrice': 141513,
        'orderedItems': [
            {
                'totalPrice': 70966,
                'pricePerPiece': 959,
                'quantity': 74,
                'productName': 'deserunt',
                'productId': '#886615'
            },
            {
                'totalPrice': 3915,
                'pricePerPiece': 435,
                'quantity': 9,
                'productName': 'laboris',
                'productId': '#623366'
            },
            {
                'totalPrice': 51282,
                'pricePerPiece': 777,
                'quantity': 66,
                'productName': 'irure',
                'productId': '#551276'
            },
            {
                'totalPrice': 15350,
                'pricePerPiece': 307,
                'quantity': 50,
                'productName': 'nostrud',
                'productId': '#930525'
            }
        ],
        'created': 'Monday, August 18, 2014 8:13 PM',
        'note': 'Esse non sunt duis dolore ullamco. Qui exercitation minim nulla quis nisi aliquip eu quis pariatur commodo aliqua tempor quis ad. Esse aliquip proident pariatur labore magna eu sint veniam. Amet veniam excepteur nisi quis consequat in quis duis dolor non adipisicing. Sint adipisicing laborum irure cillum culpa duis. Minim in anim nisi voluptate ea adipisicing in dolor proident amet amet laboris.',
        'phone': '+416 698 394 814',
        'email': 'bradley ruiz@insectus.org',
        'billingAddress': {
            'state': 'Idaho',
            'city': 'Odessa',
            'address': 'Kingston Avenue 634',
            'contact': 'Mason Alexander',
            'company': 'DUOFLEX'
        },
        'shippingAddress': {
            'state': 'American Samoa',
            'city': 'Dahlen',
            'address': 'Kosciusko Street 311',
            'contact': 'Bradley Ruiz',
            'company': 'INSECTUS'
        },
        'guid': 'cca9738c-4ad1-4000-ac30-09c918967f3e'
    }
];
