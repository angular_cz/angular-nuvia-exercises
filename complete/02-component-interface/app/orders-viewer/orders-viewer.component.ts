import { Component } from '@angular/core';
import { Order } from '../model/order';
import { orders } from '../ordersCollection';

@Component({
    selector: 'app-orders-viewer',
    templateUrl: './orders-viewer.component.html',
    styleUrls: ['./orders-viewer.component.css']
})
export class OrdersViewerComponent {

    ordersCollection = orders;
    currentOrder: Order;

    constructor() {
        this.selectOrder(this.ordersCollection[0]);
    }

    selectOrder(order: Order) {
        this.currentOrder = order;
    }
}
