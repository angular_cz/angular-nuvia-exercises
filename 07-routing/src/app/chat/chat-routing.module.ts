import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChatComponent } from './chat.component';
import { RoomsComponent } from './rooms/rooms.component';
import { ChatDetailComponent } from './chat-detail/chat-detail.component';

const routes: Routes = [
    // TODO 2.1 - přidejte routu pro chat
    // TODO 2.4 - doplňte routu roooms
    // TODO 2.5 - doplňte routu pro detail
];

@NgModule({
  // TODO 2.2 - importujte routy
  imports: [],
  exports: [RouterModule],

  // TODO 3.2 - registrujte RoomsResolver
  providers: []
})
export class ChatRoutingModule {
}
