# Cvičení - www.angular.cz #

## Co si nainstalovat
- [git](http://git-scm.com/downloads)
- doporučenou verzi [nodeJS](https://nodejs.org/en/) - 8.9 nebo větší
- [Yarn](https://yarnpkg.com/)
- Angular CLI ```npm install -g @angular/cli```

Pokud instalujete Git na Windows, zvolte možnost pro instalaci i do Windows command prompt - viz [obrázek](http://www.foryt.com/wp-content/uploads/2014/08/Git-Setup-Run-from-Windows-Command-Prompt.png) 

Stáhněte tento balíček kamkoli k sobě. 

```
git clone https://bitbucket.org/angular_cz/angular-nuvia-exercises
```

Spusťte ve staženém adresáři příkaz pro instalaci závislostí. 

Pokud používáte windows, ujistěte se, že jej spouštíte v shellu, kde máte k dispozici jak git tak node.
V případě, že si nejste jistí, můžete použít například "Git Bash" nainstalovaný spolu s gitem.


```
yarn install
```

Důležité je, aby instalace chybou neskončila. Během spuštění se mohou objevovat warningy a modul node-gyp pravděpodobně hlásí několik chyb, i přesto by měla instalace doběhnout v pořádku. Ignorujte výzvu k instalaci pythonu a visual studia.

Poté spusťte cvičení pomocí následujícího příkazu. Příkaz by neměl skončit chybou.

```
 npm start
```

Nyní, když otevřete prohlížeč na adrese [http://localhost:8000/](http://localhost:8000/), uvidíte seznam cvičení.

### Volba editoru ###

Jednotlivá cvičení budeme otevírat samostatně.

Velmi Vám doporučujeme použít editor, s podporou TypeScriptu a jeho formátování. Pokud to Váš editor neumí doporučujeme Webstorm - [https://www.jetbrains.com/webstorm/download/] (https://www.jetbrains.com/webstorm/download/) - 30 dní trial

## Použití při cvičeních: ##

Všechny potřebné nástroje a závislosti jste nainstalovali už v předchozích krocích pomocí příkazu "yarn install".

### Při dalším spouštění už si tak vystačíte s příkazem: ###
```
npm start
```
který spustí lokální server na adrese [http://localhost:8000/](http://localhost:8000/), na té také najdete další instrukce pro konkrétní cvičení.

### Možné problémy ###

####unable to connect to github.com
Pokud vidíte tuto chybovou zprávu po spuštění *bower install*
 - máte buď blokováno připojení ke githubu  - to můžete ověřit otevřením github.com v prohlížeči
 - nebo máte blokován protokol git - spusťte příkaz, který "přesměruje" protokol git po https

```
git config url."https://github.com/".insteadOf git@github.com:
git config url."https://".insteadOf git://
```

Nyní už by měl příkaz *bower install* fungovat

Pokud problémy přetrvávají, a jste uživatelem systému Windows, může zde být následující problém.

 - Nastavení výše se zapíšou do .gitconfig do domovské složky. Na Windows s profilem na vzdáleném disku však může každý ze shellů hledat home jinde.
 - Pokud je toto Váš případ, zkopírujte soubor do obou umístění, na sdílenou domovskou složku a c:\users\[name]\

Chcete-li používat konfiguraci i pro další projekty, můžete ji nastavit globálně (přidat atribut --global)

```
git config --global url."https://github.com/".insteadOf git@github.com:
git config --global url."https://".insteadOf git://
```