import { Component, HostBinding, Input, OnDestroy, OnInit } from '@angular/core';
import { TabsetComponent } from '../tabset/tabset.component';

@Component({
  selector: 'app-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['tab.component.css']
})
export class TabComponent {

  @Input() header = 'Tab';

  // TODO 2.1 - navažte vlastnost na attribut hidden
  hidden = true;

  // TODO 1.1 - injektněte nadřazenou komponentu
  constructor() {
  }

  // TODO 1.2 - použijte metody životního cyklu

  show() {
    this.hidden = false;
  }

  hide() {
    this.hidden = true;
  }
}
